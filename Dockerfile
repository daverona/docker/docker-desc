FROM node:10.19.0-alpine3.10 as builder

COPY source/package.json source/package-lock.json /
RUN npm install --global npm@latest \
  && npm install --production

FROM alpine:3.10

COPY source/ /app/
COPY --from=builder /node_modules /app/node_modules
RUN apk add --no-cache nodejs

WORKDIR /app

CMD ["node", "/app/index.js"]
