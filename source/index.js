const fs = require('fs');
let env = process.env;
let dockerHubApi = require('docker-hub-api');

dockerHubApi
    // Log in to Docker Hub
    .login(env.DOCKERHUB_USERNAME, env.DOCKERHUB_PASSWORD)
    // Make sure login token is set
    .then(function (info) {
      dockerHubApi.setLoginToken(info.token)
    })
    // Update Docker Hub repository description
    .then(function () {
      dockerHubApi.setRepositoryDescription(
          env.DOCKERHUB_NAMESPACE,
          env.DOCKERHUB_IMAGE,
          JSON.parse(JSON.stringify({
            short: env.SHORT_DESCRIPTION
                || undefined,
            full: env.FULL_DESCRIPTION
                // Read README.md for the full description in Docker Hub repository
                || fs.readFileSync('/app/README.md', {encoding: 'utf-8'})
          }))
      )
    })
