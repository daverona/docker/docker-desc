# daverona/docker-desc

[![pipeline status](https://gitlab.com/daverona/docker/docker-desc/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/docker-desc/commits/master)

This is a repository of Docker images for updating Docker Hub repository Readme and Description sections.

* GitLab repository: [https://gitlab.com/daverona/docker/docker-desc](https://gitlab.com/daverona/docker/docker-desc)
* Docker registry: [https://hub.docker.com/r/daverona/docker-desc](https://hub.docker.com/r/daverona/docker-desc)
* Available releases: [https://gitlab.com/daverona/docker/docker-desc/-/releases](https://gitlab.com/daverona/docker/docker-desc/-/releases)

If configured in CI (continuous integration), Readme and Description sections can be updated automatically.

## Quick Start

Run the container:

```bash
docker container run --rm \
  --volume $PWD/README.md:/app/README.md \
  --env DOCKERHUB_USERNAME=my-username \
  --env DOCKERHUB_PASSWORD=my-secret \
  --env DOCKERHUB_NAMESPACE=my-namespace \
  --env DOCKERHUB_IMAGE=my-image-name \
  daverona/docker-desc
```

`REAME.md` under the host current directory will be published to Readme section in Docker Hub `DOCKERHUB_NAMESPACE/DOCKERHUB_IMAGE` repository.

You should set the following environment variables:

* `DOCKERHUB_USERNAME`: username to use for Docker Hub
* `DOCKERHUB_PASSWORD`: password to use for Docker Hub
* `DOCKERHUB_NAMESPACE`: namespace of the image in Docker Hub repository
* `DOCKERHUB_IMAGE`: image name (without tag) of Docker Hub repository

The content of `README.md` file can be passed directly using environment variable `FULL_DESCRIPTION`:

```bash
docker container run --rm \
  --env FULL_DESCRIPTION="$(cat $PWD/README.md)" \
  --env DOCKERHUB_USERNAME=my-username \
  --env DOCKERHUB_PASSWORD=my-secret \
  --env DOCKERHUB_NAMESPACE=my-namespace \
  --env DOCKERHUB_IMAGE=my-image-name \
  daverona/docker-desc
```

`FULL_DESCRIPTION` takes precedence over mounted `README.md` file if any.
The environment variable `SHORT_DESCRIPTION` can be used to update Description section in the repository.

## References

* RyanTheAllmighty/Docker-Hub-API: [https://github.com/RyanTheAllmighty/Docker-Hub-API](https://github.com/RyanTheAllmighty/Docker-Hub-API)
